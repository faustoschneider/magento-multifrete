<?php
class Multifrete_MultifreteStandard_Model_Convert_Adapter_Io extends Mage_Dataflow_Model_Convert_Adapter_Io
{
    public function load()
    {
        //apaga todas as linhas antes de importar
        $regras = mage::getModel('multifretestandard/regras')->getResourceCollection();
        foreach($regras as $regra){
            $regra->delete();
        }
        parent::load();
    }
}