<?php  
    
class Multifrete_MultifreteStandard_Model_Carrier_Multifretestandard extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface
{  
    protected $_code = 'multifretestandard';
    protected $_condicoes = array();
    protected $_result = null;
    
    /**
     * Free method request
     */
    protected $_freeMethodRequest       = false;
    protected $_freeMethodRequestResult = null;
  
    public function getRegras()
    {
        $collection = Mage::getModel('multifretestandard/regras')->getResourceCollection();

        foreach($this->_condicoes as $condicao)
        {
            $fields = $condicao[0];

            if($condicao[0] == 'pais' || $condicao[0] == 'estado' || $condicao[0] == 'cidade' || $condicao[0] == 'website'){
                $condicoes = array(
                                array($condicao[1] => array($condicao[2])),
                                array('eq' => '*')
                            );
            } else {
                $condicoes = array($condicao[1] => array($condicao[2]));
            }

            $collection->addFieldToFilter($fields,$condicoes);
        }

        $collection->getSelect()->order(array('ordem ASC'));

        return $collection;
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $this->_result = Mage::getModel('shipping/rate_result');

        $cep = preg_replace("/[^0-9]+/", "", $request->getDestPostcode());

        if(!$this->checkCEP($cep))
        {
            $this->_throwError('O CEP preenchido não é válido.', 'O CEP preenchido é inválido!');
            return $this->_result;
        }

        $peso           = $request->getPackageWeight();            
        $freeMethod     = $this->getConfigData('free_method');

        $this->_condicoes[] = array('pais', 'finset', $request->getDestCountryId());
        $this->_condicoes[] = array('estado', 'finset', $request->getDestRegionCode());
        $this->_condicoes[] = array('cep_de', 'lteq', $cep);
        $this->_condicoes[] = array('cep_ate', 'gteq', $cep);
        $this->_condicoes[] = array('website', 'finset', $request->getWebsiteId());
        $this->_condicoes[] = array('cidade', 'like', $request->getDestCity());
        
        $this->_condicoes[] = array('peso_de', 'lteq', $peso);
        $this->_condicoes[] = array('peso_ate', 'gteq', $peso);

        //verifica quais regras se encaixam nas condições adicionadas acima
        $regras = $this->getRegras();
        $menor  = 999;
        $this->_freeMethod = '';

        foreach($regras as $regra)
        {
            if($regra->getValor() <= $menor){
                $this->_freeMethod = $regra->getCodigo();
                $menor = $regra->getValor();
            }
        }
 
        // Use descont codes
        $this->_updateFreeMethodQuote($request);

        foreach($regras as $regra)
        {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));
            $method->setMethod($regra->getCodigo());
            $method->setMethodTitle($regra->getTitulo());
            $method->setPrice($regra->getValor());
            $method->setCost($regra->getCusto());
            
            if($cep >= '90000000' && $cep <= '99999999') 
            {
                if($regra->getCodigo() == $this->_freeMethod)
                {
                    $method->setPrice('5.00');
                    $method->setCost('5.00');
                }                        
            }

            if($request->getFreeShipping() && $regra->getCodigo() == $this->_freeMethod)
            {
                $method->setPrice('0.00');
                $method->setCost('0.00');
            }   

            // if($cep >= '90000000' && $cep <= '99999999') 
            // {
            // //if(($cep >= '90000000' && $cep <= '99999999') && ($request->getPackageValueWithDiscount() >= 99) {
            //     if($regra->getCodigo() == $free_method)
            //     {
            //         $method->setPrice(0.00);
            //     }                        
            // }

            $this->_result->append($method);
        }
              
        return $this->_result;
    }

    /**
     * Allows free shipping when all product items have free shipping (promotions etc.)
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return void
     */
    protected function _updateFreeMethodQuote($request)
    {
        $freeShipping = false;
        $items = $request->getAllItems();
        $c = count($items);
        for ($i = 0; $i < $c; $i++) {
            if ($items[$i]->getProduct() instanceof Mage_Catalog_Model_Product) {
                if ($items[$i]->getFreeShipping()) {
                    $freeShipping = true;
                } else {
                    return;
                }
            }
        }
        if ($freeShipping) {
            $request->setFreeShipping(true);
        }
    }

    /**
     * Generate free shipping for a product
     *
     * @param string $freeMethod
     * @return void
     */
    protected function _setFreeMethodRequest($freeMethod)
    {

        mage::log($freeMethod, null, 'freeMethod.log');

        // Set request as free method request
        $this->_freeMethodRequest = true;
        $this->_freeMethodRequestResult = Mage::getModel('shipping/rate_result');

        $this->_postMethods = $freeMethod;
        $this->_postMethodsExplode = array($freeMethod);       

        // Tranform free shipping weight
        if($this->getConfigData('weight_type') == 'gr')
        {
            $this->_freeMethodWeight = number_format($this->_freeMethodWeight/1000, 2, '.', '');
        }
        
        $this->_packageWeight = $this->_freeMethodWeight;
        $this->_pacWeight = $this->_freeMethodWeight;
    }

    protected function checkCEP($cep)
    {
        if($cep){
            $piped = file_get_contents("https://viacep.com.br/ws/".$cep."/piped");
            
            if($piped != 'erro:true')
                return true;
        }

        return false;
    }

    /**
     * Throw error
     *
     * @param $message string
     * @param $log     string
     * @param $line    int
     * @param $custom  string
     * @return void
     */
    protected function _throwError($message, $log = null, $line = 'NO LINE', $custom = null)
    {
        $this->_result = null;
        $this->_result = Mage::getModel('shipping/rate_result');

        // Get error model
        $error = Mage::getModel('shipping/rate_result_error');
        $error->setCarrier($this->_code);
        $error->setCarrierTitle($this->getConfigData('title'));
        $error->setErrorMessage($message);
        
        // Apend error
        $this->_result->append($error);
    }

	/**
	 * Get allowed shipping methods
	 *
	 * @return array
	 */
	public function getAllowedMethods()
	{
		return array($this->_code=>$this->getConfigData('name'));
	}

    // public function isTrackingAvailable() 
    // {
    //     return true;
    // }

    // public function getTrackingInfo($tracking) 
    // {
    //     $result = $this->getTracking($tracking);
    //     if ($result instanceof Mage_Shipping_Model_Tracking_Result){
    //         if ($trackings = $result->getAllTrackings()) {
    //                 return $trackings[0];
    //         }
    //     } elseif (is_string($result) && !empty($result)) {
    //         return $result;
    //     }
    //     return false;
    // }

    // public function getTracking($trackings) 
    // {
    //     $this->tracking = Mage::getModel('shipping/tracking_result');
    
    //     foreach ((array) $trackings as $code) 
    //     {
    //         if(is_numeric($code))
    //             $this->_getTrackingJadLog($code);
    //         if(!is_numeric($code))
    //             $this->_getTrackingCorreios($code);
    //     }

    //     return $this->tracking;
    // }

    // protected function _getTrackingCorreios($code)
    // {
    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-20',
    //         'deliverytime'      => '14:44:55',
    //         'activity'          => 'Postado',
    //         'deliverylocation'  => 'Novo Hamburgo (QG Trapo Tri)',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-21',
    //         'deliverytime'      => '10:00:55',
    //         'activity'          => 'Em trânsito',
    //         'deliverylocation'  => 'São Leopoldo',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-21',
    //         'deliverytime'      => '15:00:00',
    //         'activity'          => 'Em trânsito',
    //         'deliverylocation'  => 'Porto Alegre (CD JADLOG)',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-22',
    //         'deliverytime'      => '08:45:00',
    //         'activity'          => 'Saiu para entrega ao destinatário',
    //         'deliverylocation'  => 'Canoas',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-22',
    //         'deliverytime'      => '18:00:00',
    //         'activity'          => 'Entregue ao destinatário',
    //         'deliverylocation'  => 'Canoas',
    //     );


    //     $track['progressdetail'] = $progress;
    //     $tracking = Mage::getModel('shipping/tracking_result_status');
    //     $tracking->setTracking($code);
    //     $tracking->setCarrier($this->_code);
    //     $tracking->setCarrierTitle('Entrega por CORREIOS');
    //     $tracking->addData($track);

    //     $this->tracking->append($tracking);
    // }

    // protected function _getTrackingJadLog($code)
    // {
    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-20',
    //         'deliverytime'      => '14:44:55',
    //         'activity'          => 'Postado',
    //         'deliverylocation'  => 'Novo Hamburgo (QG Trapo Tri)',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-21',
    //         'deliverytime'      => '10:00:55',
    //         'activity'          => 'Em trânsito',
    //         'deliverylocation'  => 'São Leopoldo',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-21',
    //         'deliverytime'      => '15:00:00',
    //         'activity'          => 'Em trânsito',
    //         'deliverylocation'  => 'Porto Alegre (CD JADLOG)',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-22',
    //         'deliverytime'      => '08:45:00',
    //         'activity'          => 'Saiu para entrega ao destinatário',
    //         'deliverylocation'  => 'Canoas',
    //     );

    //     $progress[] = array(
    //         'deliverydate'      => '2016-06-22',
    //         'deliverytime'      => '18:00:00',
    //         'activity'          => 'Entregue ao destinatário',
    //         'deliverylocation'  => 'Canoas',
    //     );


    //     $track['progressdetail'] = $progress;
    //     $tracking = Mage::getModel('shipping/tracking_result_status');
    //     $tracking->setTracking($code);
    //     $tracking->setCarrier($this->_code);
    //     $tracking->setCarrierTitle('Entrega por JADLOG');
    //     $tracking->addData($track);

    //     $this->tracking->append($tracking);
    // }
                
}  
