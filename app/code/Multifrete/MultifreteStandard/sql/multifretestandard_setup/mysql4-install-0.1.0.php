<?php
$installer = $this;
$installer->startSetup();

$table = $installer->getTable('multifretestandard/regras');
$sql=<<<SQLTEXT
DROP TABLE IF EXISTS $table;
CREATE TABLE $table (id_regra int not null auto_increment, titulo varchar(250),  peso_de varchar(250), peso_ate varchar(250), pais varchar(250), estado varchar(500), cidade varchar(250),  cep_de int(12),  cep_ate int(12), valor varchar(250) , custo varchar(250), website varchar(250),  store varchar(250),  view varchar(250), primary key(id_regra));
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 