<?php


class Multifrete_MultifreteStandard_Block_Adminhtml_Regras extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_regras";
	$this->_blockGroup = "multifretestandard";
	$this->_headerText = Mage::helper("multifretestandard")->__("Regras Manager");
	$this->_addButtonLabel = Mage::helper("multifretestandard")->__("Add New Item");
	parent::__construct();
	
	}

}